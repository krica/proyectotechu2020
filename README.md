# Banca KRIVA - KRISS Y VANESA

Este proyecto se basa en la plantilla de polymer-2-starter-kit, tecnología basado en web components.
“Banca Krova” es una web de banca en el cual tendremos inicio de sesión (Entrar y salir), mantenimiento de usuarios, mantenimiento de cuentas y mantenimiento de movimientos. 

### Repositorio

[Repositorio!](https://bitbucket.org/krica/proyectotechu2020/src/master/)

### Introduccion
Este proyecto se basa en la plantilla de polymer-2-starter-kit, tecnología basado en web components.
“Banca KrIva” es una web de banca en el cual tendremos inicio de sesión (Entrar y salir), mantenimiento de usuarios, mantenimiento de cuentas y mantenimiento de movimientos. 

##### Lista de comandos para puesta en marcha

Para poner en marcha la aplicación tendremos que realizar los siguientes comandos:

First, install [Polymer CLI](https://github.com/Polymer/polymer-cli) using
[npm](https://www.npmjs.com) (we assume you have pre-installed [node.js](https://nodejs.org)).

    npm install -g polymer-cli

Second, install [Bower](https://bower.io/) using [npm](https://www.npmjs.com)

    npm install -g bower

##### Initialize project from template

    mkdir my-app
    cd my-app
    polymer init polymer-2-starter-kit

### Start the development server

This command serves the app at `http://127.0.0.1:8081` and provides basic URL
routing for the app:

    polymer serve

### Componente

npm install -g bower

bower install --save iron-ajax@2.1.3

bower install --save paper-button@2.1.3

bower install --save SieBrum/brum-global-variable

bower install --save paper-input@2.1.3

bower install --save iron-localstorage@2.1.3

polymer serve 


### Autores

KRISS MINANO ROSAS - VANEZA ORTIZ LOBATO
